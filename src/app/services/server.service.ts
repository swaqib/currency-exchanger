import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root',
})

export class ServerService {

    constructor(private http: HttpClient) { }

    exchangeCurrency(from, to) {
        from: from;
        to: to;
        let key = '4686f2101a481ab13b5b';
        let query = from + '_' + to;

        return this.http.get(`${environment.apiBaseURL}/api/v6/convert?q=` + query + '&apiKey=' + key)
    }
    getList(){
        let key = '4686f2101a481ab13b5b';
        return this.http.get(`${environment.apiBaseURLlist}/api/v7/currencies?apiKey=` + key)
    }

}