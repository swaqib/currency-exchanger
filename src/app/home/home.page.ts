import { ServerService } from './../services/server.service';
import { Component } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  value: any;
  result = null;
  lists: any;
  listing:any;
  arr = [];

  toCurrency = [
    { 'currency': 'US Dolllar', 'symbol': 'USD' , 'index': 0},
    { 'currency': 'Euro', 'symbol': 'EUR' , 'index': 1},
    { 'currency': 'Australian Dolllar', 'symbol': 'AUD', 'index': 2 },
    { 'currency': 'Malaysian Ringgit', 'symbol': 'MYR', 'index': 3 },
  ];

  currentToCurrency: any = this.toCurrency[0];

  fromCurrency = [
    { 'currency': 'US Dolllar', 'symbol': 'USD', 'index': 0},
    { 'currency': 'Euro', 'symbol': 'EUR', 'index': 1 },
    { 'currency': 'Australian Dolllar', 'symbol': 'AUD', 'index': 2},
    { 'currency': 'Malaysian Ringgit', 'symbol': 'MYR' , 'index': 3},
  ]
  currentFromCurrency: any = this.fromCurrency[0];


  constructor(
    private server: ServerService
  ) { }

  ngOnInit(): void { }

  ionViewDidEnter(){
    this.currencyList();
    // let arr = this.currencyList();
    // console.log("arr is => ",this.arr);
  }

  async exchangeCur() {
    this.server.exchangeCurrency(this.currentFromCurrency.value, this.currentToCurrency.value).subscribe(
      (res: any) => {
        console.log("Exchange Currency Success", res)
        this.result = res;
      }, error => {
        console.log("Exchange Currency Error", error)
      }
    )
  }

  async currencyList() {
    this.server.getList().subscribe(
      (data: any) => {
        this.listing = data.results;
        console.log("listing => ",this.listing);
        for (let i = 0; i < this.listing.length; i++) {
          this.lists.push(this.listing[i]);
        };
        console.log("lists => ",this.lists);
      }, error => {
        console.log("Currency List Error", error)
      }
    )
  }
}
